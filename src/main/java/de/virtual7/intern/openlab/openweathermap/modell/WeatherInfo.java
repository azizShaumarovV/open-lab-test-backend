package de.virtual7.intern.openlab.openweathermap.modell;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.sql.Timestamp;
import lombok.Data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@Embeddable
public class WeatherInfo {

  /** Time of data forecasted, unix, UTC.for the next 3 hours. */
  @Column
  private Timestamp weatherDateTime;

  // TODO double -> auf 2 nachkommastellen verkürzen
  /** The Temperature (in Celsius -> if flag is set, default celvin). */
  @Column
  private double temperature;

  /** Humidity, % */
  @Column
  private int humidity;

  /** Atmospheric pressure on the sea level, hPa */
  @Column
  private double pressure;

  /** Weather codes -> look at the TODO:table/enum */
  @Column
  private int[] weatherCodes;

  /** Cloudiness, % */
  @Column
  private int clouds;

  /** Rain volume for the last 3 hours */
  @Column
  private double rain;

  /** Snow volume for the last 3 hours */
  @Column
  private double snow;

  /** Wind speed, meter/sec */
  @Column
  private double windspeed;

  @JsonProperty("dt")
  public void setWeatherDateTime(long weatherDateTime) {
    this.weatherDateTime = new java.sql.Timestamp(weatherDateTime * 1000);
  }

  @JsonProperty("main")
  public void setInfoFromMainTag(JsonNode main) {
    this.temperature = main.path("temp").floatValue();
    this.humidity = main.path("humidity").intValue();
    this.pressure = main.path("pressure").doubleValue();
  }

  @JsonProperty("weather")
  public void setInfoFromWeatherTag(JsonNode weather) {
    this.weatherCodes = new int[weather.size()];
    weather.forEach((JsonNode node) -> this.weatherCodes[node.asInt()] = node.path("id").intValue());
  }

  @JsonProperty("clouds")
  public void setInfoFromCloudsTag(JsonNode clouds) {
    this.clouds = clouds.path("all").intValue();
  }

  @JsonProperty("rain")
  public void setInfoFromRainTag(JsonNode rain) {
    this.rain = rain.path("3h").doubleValue();
  }

  @JsonProperty("snow")
  public void setInfoFromSnowTag(JsonNode snow) {
    this.snow = snow.path("3h").doubleValue();
  }

  @JsonProperty("wind")
  public void setInfoFromWindTag(JsonNode wind) {
    this.windspeed = wind.path("speed").doubleValue();
  }

  public Timestamp getWeatherDateTime() {
    return this.weatherDateTime;
  }

  public double getTemperature() {
    return this.temperature;
  }

  public int getHumidity() {
    return this.humidity;
  }

  public double getPressure() {
    return this.pressure;
  }

  public int[] getWeatherCodes() {
    return this.weatherCodes;
  }

  public int getCloudiness() {
    return this.clouds;
  }

  public double getRainVolume() {
    return this.rain;
  }

  public double getSnowVolume() {
    return this.snow;
  }

  public double getWindSpeed() {
    return this.windspeed;
  }

}
