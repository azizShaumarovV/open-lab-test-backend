package de.virtual7.intern.openlab.openweathermap.exceptions;

public class WeatherCodeNotFoundException extends Exception {

    public WeatherCodeNotFoundException(String message) {
        super(message);
    }
}
