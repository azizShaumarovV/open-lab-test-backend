package de.virtual7.intern.openlab.openweathermap.data;

import java.util.List;
import de.virtual7.intern.openlab.openweathermap.modell.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CityApiRepository extends JpaRepository<City, Long> {

    List<City> findByNameIgnoreCase(String name);

    List<City> findByNameAndCountryAllIgnoreCase(String name, String country);
}
