package de.virtual7.intern.openlab.openweathermap.connectors.imp;

import de.virtual7.intern.openlab.openweathermap.exceptions.LimitApiCallsReachedException;
import org.springframework.http.ResponseEntity;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherCity;
import de.virtual7.intern.openlab.openweathermap.connectors.WeatherApi;
import org.springframework.web.client.RestTemplate;

import java.sql.Timestamp;

/**
 * The implementation of request from openweathermap.org API.
 */

public class WeatherApiImpl implements WeatherApi {

    private final static String weatherURL = "http://api.openweathermap.org/data/2.5/forecast?id=";
    private final static String appid = "&appid=50b30f6a124958188352a89b94452517";
    private final static String metric = "&units=metric";

    private static WeatherApiImpl singleton;
    private final static int MAXCALLALLOWED = 60;

    private int callCounter;
    private Timestamp lastCallTime;

    private WeatherApiImpl() {
        this.callCounter = 0;
        this.lastCallTime = new Timestamp(System.currentTimeMillis());
    }

    public WeatherCity getWeatherInfoByCityId(long id) throws LimitApiCallsReachedException {

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<WeatherCity> response; // TODO internet connection lost to weather api

        // checks max call request to server
        if (callCounter < MAXCALLALLOWED) {

            response = restTemplate.getForEntity(weatherURL + id + appid + metric, WeatherCity.class);

            this.lastCallTime = new Timestamp(System.currentTimeMillis());
            ++this.callCounter;

        } else {

            if (this.lastCallTime.after(new Timestamp(System.currentTimeMillis() - 60000L))) {
                throw new LimitApiCallsReachedException("Allowed only 60 server calls in one minute");
            } else {
                System.out.println("Security initial");
                response = restTemplate.getForEntity(weatherURL + id + appid + metric, WeatherCity.class);
                this.callCounter = 0;
                this.lastCallTime = new Timestamp(System.currentTimeMillis());
            }

        }

        return response.getBody();
    }

    public static WeatherApiImpl getWeatherApi() {
        if (singleton == null) {
            return new WeatherApiImpl();
        }
        return singleton;
    }

}
