package de.virtual7.intern.openlab.openweathermap.modell;

import java.sql.Timestamp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import lombok.Data;

/**
 * The Class WeatherCity.
 */

@Entity
@Table(name = "weather_city")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class WeatherCity {

    /* The id. given by openweathermap.org */
    @Id
    @Column(name = "city_id")
    private long id;


    /* Contains forecast for 5 days / 3 hours => 40 elements
     * also date information // TOD: date hierher rausholen, und "40" aus json auslesen
     */
    @Column
    @ElementCollection
    @CollectionTable(
            name = "weather_info",
            joinColumns = @JoinColumn(name = "city_weath_id")
    )
    @OrderColumn
    @JsonProperty("list")
    private WeatherInfo[] weatherInfo;


    @JsonProperty("city")
    public void setInfoFromCityTag(JsonNode city) {
        this.id = city.path("id").longValue();
    }

    public Timestamp getForecastBeginTime() {
        return weatherInfo[0].getWeatherDateTime();
    }

    public Timestamp getForecastEndTime() {
        return weatherInfo[39].getWeatherDateTime();
    }

    public long getId() {
        return this.id;
    }

    // under condition: datetime in range
    public WeatherInfo getWeatherDetails(Timestamp datetime) {
        for (int i = 1; i < 39; i++) {
            if (weatherInfo[i].getWeatherDateTime().after(datetime)) {
                return weatherInfo[i - 1];
            }
        }
        return weatherInfo[39];
    }

}
