package de.virtual7.intern.openlab.openweathermap.exceptions;

public class ForecastNotReachableException extends Exception {

    public ForecastNotReachableException(String message) {
        super(message);
    }
}
