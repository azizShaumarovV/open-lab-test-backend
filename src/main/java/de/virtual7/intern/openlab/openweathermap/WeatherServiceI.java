package de.virtual7.intern.openlab.openweathermap;

import de.virtual7.intern.openlab.openweathermap.exceptions.*;
import de.virtual7.intern.openlab.openweathermap.modell.City;

import java.sql.Timestamp;

public interface WeatherServiceI {

    /**
     * The Temperature in Celsius
     */
    double getTemperature(Timestamp datetimetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Humidity, in %
     */
    int getHumidity(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Atmospheric pressure on the sea level, hPa
     */
    double getPressure(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Cloudiness, in %
     */
    int getCloudiness(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Rain volume for the last 3 hours
     */
    double getRainVolume(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Snow volume for the last 3 hours
     */
    double getSnowVolume(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Wind speed, meter/sec
     */
    double getWindSpeed(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException;

    /**
     * Weather condition, verbal
     */
    WeatherCondition getWeatherCondition(Timestamp datetime) throws PastDateTimeException, CityNotFoundException, ForecastNotReachableException, LimitApiCallsReachedException, WeatherCodeNotFoundException;

    void setCity(String city) throws CityNotFoundException, LimitApiCallsReachedException;

    void setCity(String city, String country) throws CityNotFoundException, LimitApiCallsReachedException;

    void setCity(City city) throws CityNotFoundException, LimitApiCallsReachedException;

    void setCity(long cityID) throws CityNotFoundException, LimitApiCallsReachedException;


    enum WeatherCondition {
        // TODO: Details hinzufügen!!!???
        // https://openweathermap.org/weather-conditions
        THUNDERSTORM(200, 232), DRIZZLE(300, 321), RAIN(500, 531),
        SNOW(600, 622), ATMOSPHERE(701, 781), CLEAR(800, 800),
        CLOUDS(801, 804);

        private int startCode;
        private int endCode;

        private WeatherCondition(int startCode, int endCode) {
            this.startCode = startCode;
            this.endCode = endCode;
        }

        public static WeatherCondition getWthrCondtionByCode(int code) throws WeatherCodeNotFoundException {
            if (THUNDERSTORM.startCode <= code && code <= THUNDERSTORM.endCode) {
                return THUNDERSTORM;
            } else if (DRIZZLE.startCode <= code && code <= DRIZZLE.endCode) {
                return DRIZZLE;
            } else if (RAIN.startCode <= code && code <= RAIN.endCode) {
                return RAIN;
            } else if (SNOW.startCode <= code && code <= SNOW.endCode) {
                return SNOW;
            } else if (ATMOSPHERE.startCode <= code && code <= ATMOSPHERE.endCode) {
                return ATMOSPHERE;
            } else if (CLEAR.startCode <= code && code <= CLEAR.endCode) {
                return CLEAR;
            } else if (CLOUDS.startCode <= code && code <= CLOUDS.endCode) {
                return CLOUDS;
            }
            throw new WeatherCodeNotFoundException("Wrong weather code");
        }

    }
}
