package de.virtual7.intern.openlab.openweathermap;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.Optional;
import java.util.List;

import de.virtual7.intern.openlab.openweathermap.connectors.imp.WeatherApiImpl;
import de.virtual7.intern.openlab.openweathermap.connectors.WeatherApi;
import de.virtual7.intern.openlab.openweathermap.exceptions.*;
import de.virtual7.intern.openlab.openweathermap.modell.City;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherCity;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherInfo;
import de.virtual7.intern.openlab.openweathermap.data.CacheCityWeatherRepository;
import de.virtual7.intern.openlab.openweathermap.data.CityApiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class WeatherService implements WeatherServiceI {

    /* The city connectors ID repository. (All cities from openweathermap)  */
    @Autowired
    private CityApiRepository cityApiRepository;

    /* The city Weather information cache repository. */
    @Autowired
    private CacheCityWeatherRepository cacheCityWeatherRepository;

    /*  */
    private WeatherApi openweatherapi;

    /* Weather Information of the city */
    private WeatherCity weatherCity;


    protected WeatherService() {
        openweatherapi = WeatherApiImpl.getWeatherApi();
    }

    // TODO handling with cities list... -> now just the first one is tooken
    public void setCity(String city) throws CityNotFoundException, LimitApiCallsReachedException {

        List<City> cities = this.cityApiRepository.findByNameIgnoreCase(city);

        if (cities.isEmpty()) {
            throw new CityNotFoundException("City name not founded, please look at the cities list");
        }
        Optional<WeatherCity> ctWeatherCache = this.cacheCityWeatherRepository.findById(cities.get(0).getId());
        this.weatherCity = ctWeatherCache.isPresent() ? ctWeatherCache.get() : this.openweatherapi.getWeatherInfoByCityId(cities.get(0).getId());

    }

    public void setCity(String city, String country) throws CityNotFoundException, LimitApiCallsReachedException {

        List<City> cities = this.cityApiRepository.findByNameAndCountryAllIgnoreCase(city, country);

        if (cities.isEmpty()) {
            throw new CityNotFoundException("City/country name not founded, please look at the cities list");
        }
        Optional<WeatherCity> ctWeatherCache = this.cacheCityWeatherRepository.findById(cities.get(0).getId());
        this.weatherCity = ctWeatherCache.isPresent() ? ctWeatherCache.get() : this.openweatherapi.getWeatherInfoByCityId(cities.get(0).getId());

    }

    public void setCity(City city) throws CityNotFoundException, LimitApiCallsReachedException {
        this.setCity(city.getId());
    }

    public void setCity(long cityId) throws CityNotFoundException, LimitApiCallsReachedException {
        Optional<WeatherCity> ctWeatherCache = this.cacheCityWeatherRepository.findById(cityId);
        this.weatherCity = ctWeatherCache.isPresent() ? ctWeatherCache.get() : this.openweatherapi.getWeatherInfoByCityId(cityId);
    }

    /* The Temperature in Celsius */
    public double getTemperature(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getTemperature();
    }

    /* Humidity, in % */
    public int getHumidity(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getHumidity();
    }

    /* Atmospheric pressure on the sea level, hPa */
    public double getPressure(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getPressure();
    }

    /* Cloudiness, in % */
    public int getCloudiness(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getCloudiness();
    }

    /* Rain volume for the last 3 hours */
    public double getRainVolume(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getRainVolume();
    }

    /* Snow volume for the last 3 hours */
    public double getSnowVolume(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getSnowVolume();
    }

    /* Wind speed, meter/sec */
    public double getWindSpeed(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {
        inDateRange(datetime);
        return this.weatherCity.getWeatherDetails(datetime).getWindSpeed();
    }

    /* Weather condition, verbal */
    public WeatherCondition getWeatherCondition(Timestamp datetime) throws CityNotFoundException, PastDateTimeException,
            ForecastNotReachableException, WeatherCodeNotFoundException, LimitApiCallsReachedException {
        inDateRange(datetime);
        int[] weatherCode = this.weatherCity.getWeatherDetails(datetime).getWeatherCodes();
        return WeatherCondition.getWthrCondtionByCode(weatherCode[0]);
    }

    private void inDateRange(Timestamp datetime) throws PastDateTimeException, CityNotFoundException,
            ForecastNotReachableException, LimitApiCallsReachedException {

        isForecastPssbl(datetime);

        if (datetime.before(this.weatherCity.getForecastBeginTime())) {
            throw new PastDateTimeException("Datetime is in the past");
        }
        if (datetime.after(this.weatherCity.getForecastEndTime())) {
            this.weatherCity = this.openweatherapi.getWeatherInfoByCityId(this.weatherCity.getId());
        }
    }

    /* checks for reachablty of weather forecast */
    private void isForecastPssbl(Timestamp datetime) throws CityNotFoundException, ForecastNotReachableException {
        if (this.weatherCity == null) {
            throw new CityNotFoundException("Please set a city");
        }

        Calendar currentDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        Timestamp currentTime = new Timestamp(new java.util.Date().getTime());
        currentDate.setTimeInMillis(currentTime.getTime());
        currentDate.add(Calendar.DATE, 5);
        if (datetime.after(currentDate.getTime())) {
            throw new ForecastNotReachableException("The given datetime is more than 5 days from now");
        }
    }

    ///////// For Develop Phase Controller Access
    public WeatherCity getWeatherCity() {
        return this.weatherCity;
    }

    public WeatherInfo getWeatherInfo(Timestamp datetime) throws ForecastNotReachableException {
        if (datetime.after(this.weatherCity.getForecastBeginTime()) && datetime.before(this.weatherCity.getForecastEndTime())) {
            return this.weatherCity.getWeatherDetails(datetime);
        } else {
            new ForecastNotReachableException("Forecast for this datetime is not reachable");
        }
        return null;
    }
}
