package de.virtual7.intern.openlab.openweathermap.modell;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Size;

import lombok.Data;

/*
 * The Class City (openweathermap.org).
 * main use: Mapping city.json throug JPA to DB
 */

@Entity
@Table(name = "tbl_city_openweathermap")
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class City {

  /** The id. given by openweathermap.org */
  @Id
  private long id;

  /** The city name. */
  @Size(max=100)
  private String name;

  /** The country abbreviation. */
  @Size(max=5)
  private String country;

  protected City() {}

  public City(long id, String name, String country) {
    this.id = id;
    this.name = name;
    this.country = country;
  }

  public long getId() {
    return this.id;
  }


}
