package de.virtual7.intern.openlab.openweathermap.data;

import de.virtual7.intern.openlab.openweathermap.modell.WeatherCity;
import org.springframework.data.jpa.repository.JpaRepository;

// TODO: cache function -> delete data
public interface CacheCityWeatherRepository extends JpaRepository<WeatherCity, Long> {

}
