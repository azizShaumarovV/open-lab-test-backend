package de.virtual7.intern.openlab.openweathermap.converter;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.virtual7.intern.openlab.openweathermap.modell.City;

import java.io.File;
import java.io.IOException;

/**
 * The Class CityRequestIDs.
 */
public class CityRequestIDs {

    /**
     * Convert to city entity / Array from given Json File (path).
     *
     * @param todoAsJson the city as json
     * @return the todo
     */
    public static City[] getCities(String path) {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        City[] cities = null;
        try {
            cities = objectMapper.readValue(new File(path), City[].class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cities;
    }
}
