package de.virtual7.intern.openlab.openweathermap.connectors;

import de.virtual7.intern.openlab.openweathermap.exceptions.CityNotFoundException;
import de.virtual7.intern.openlab.openweathermap.exceptions.LimitApiCallsReachedException;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherCity;

public interface WeatherApi {

    /**
     * Gets the city weather Information (JSON -> String) by ID.
     * from api.openweathermap.org
     *
     * @param id the id
     * @return String
     */
    WeatherCity getWeatherInfoByCityId(long id) throws CityNotFoundException, LimitApiCallsReachedException;

}
