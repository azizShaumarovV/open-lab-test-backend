package de.virtual7.intern.openlab.openweathermap.exceptions;

public class PastDateTimeException extends Exception {

    public PastDateTimeException(String message) {
        super(message);
    }
}
