package de.virtual7.intern.openlab.openweathermap.controller;

import de.virtual7.intern.openlab.openweathermap.WeatherService;
import de.virtual7.intern.openlab.openweathermap.api.WeatherControllerApi;
import de.virtual7.intern.openlab.openweathermap.exceptions.CityNotFoundException;
import de.virtual7.intern.openlab.openweathermap.exceptions.ForecastNotReachableException;
import de.virtual7.intern.openlab.openweathermap.exceptions.LimitApiCallsReachedException;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherCity;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;

import java.sql.Timestamp;

@Controller
public class WeatherController implements WeatherControllerApi {

    @Autowired
    WeatherService weatherService;

    @Override
    public ResponseEntity<WeatherCity> getWholeWeatherInformation(@PathVariable String city){
        try {
            weatherService.setCity(city);
        } catch(CityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch(LimitApiCallsReachedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(weatherService.getWeatherCity(), HttpStatus.OK);
    }

    @Override
    public ResponseEntity<WeatherCity> getWholeWeatherInformation(@PathVariable String city, @PathVariable String country) {
        try {
            weatherService.setCity(city, country);
        } catch(CityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch(LimitApiCallsReachedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(weatherService.getWeatherCity(), HttpStatus.OK);
    }




    @Override
    public ResponseEntity<WeatherInfo> getWeatherForecast(@PathVariable String city, @PathVariable long datetime) {
        try {
            weatherService.setCity(city);
            return ResponseEntity.ok(weatherService.getWeatherInfo(new Timestamp(datetime)));
        } catch(CityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch(LimitApiCallsReachedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch(ForecastNotReachableException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<WeatherInfo> getWeatherForecast(@PathVariable String city, @PathVariable String country, @PathVariable long datetime) {
        try {
            weatherService.setCity(city, country);
            return ResponseEntity.ok(weatherService.getWeatherInfo(new Timestamp(datetime)));
        } catch(CityNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch(LimitApiCallsReachedException e) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        } catch(ForecastNotReachableException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
