package de.virtual7.intern.openlab.openweathermap.exceptions;

public class CityNotFoundException extends Exception {

    public CityNotFoundException(String message) {
        super(message);
    }
}
