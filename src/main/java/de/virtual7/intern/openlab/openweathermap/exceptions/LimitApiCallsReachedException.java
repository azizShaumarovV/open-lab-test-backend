package de.virtual7.intern.openlab.openweathermap.exceptions;

public class LimitApiCallsReachedException extends Exception {

    public LimitApiCallsReachedException(String message) {
        super(message);
    }
}
