package de.virtual7.intern.openlab.openweathermap.api;

import de.virtual7.intern.openlab.openweathermap.modell.WeatherCity;
import de.virtual7.intern.openlab.openweathermap.modell.WeatherInfo;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// For Test/Develop Phase!!!

public interface WeatherControllerApi {

    // get whole weather forecast for 5 day
    // @GetMapping(value = "/api/weather/{cityID}", produces = MediaType.APPLICATION_JSON_VALUE)
    // ResponseEntity<WeatherCity> getWholeWeatherInformation(@PathVariable long cityID);

    // get whole weather forecast for 5 days
    @GetMapping(value = "/api/weather/{city}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WeatherCity> getWholeWeatherInformation(@PathVariable String city);

    // get whole weather forecast for 5 days
    @GetMapping(value = "/api/weather/{city}/{country}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WeatherCity> getWholeWeatherInformation(@PathVariable String city, @PathVariable String country);



    // get forecast for 3 hours from this datetime
    // @GetMapping(value = "/api/weather/{cityID}/{datetime}", produces = MediaType.APPLICATION_JSON_VALUE)
    // ResponseEntity<WeatherInfo> getWeatherForecast(@PathVariable long cityID, @PathVariable long datetime);

    // get forecast for 3 hours from this datetime
    @GetMapping(value = "/api/weather/{city}/datetime/{datetime:[\\d]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WeatherInfo> getWeatherForecast(@PathVariable String city, @PathVariable long datetime);

    // get forecast for 3 hours from this datetime
    @GetMapping(value = "/api/weather/{city}/{country}/datetime/{datetime:[\\d]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WeatherInfo> getWeatherForecast(@PathVariable String city, @PathVariable String country, @PathVariable long datetime);
}
