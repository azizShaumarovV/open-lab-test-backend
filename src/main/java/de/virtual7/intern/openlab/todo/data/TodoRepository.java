package de.virtual7.intern.openlab.todo.data;

import de.virtual7.intern.openlab.todo.modell.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface TodoRepository extends JpaRepository<Todo, Long> {


    List<Todo> findAllByDescription(String description);

    List<Todo> findAllByTodoDateTime(Date todoDateTime);

    List<Todo> findAllByTodoDateTimeBetween(Date todoDateTimeStart, Date todoDateTimeEnd);

    @Query("select a from Todo a where a.todoDateTime <= :todoDateTime")
    List<Todo> findAllWithTodoDateTimeBefore(@Param("todoDateTime") Date todoDateTime);

    @Query("select a from Todo a where a.todoDateTime >= :todoDateTime")
    List<Todo> findAllWithTodoDateTimeAfter(@Param("todoDateTime") Date todoDateTime);
}
