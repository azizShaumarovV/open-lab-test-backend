package de.virtual7.intern.openlab.todo.controller;

import de.virtual7.intern.openlab.todo.api.TodoControllerApi;
import de.virtual7.intern.openlab.todo.data.TodoRepository;
import de.virtual7.intern.openlab.todo.modell.Todo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;
import java.util.List;
import java.util.Optional;


@Controller
public class TodoController implements TodoControllerApi {

	/* The todo repository. */
	@Autowired
	private TodoRepository todoRepository;

	@Override
	public String fallbackMethod(){
		return "error_page";
	}

	@Override
	public ResponseEntity<?> newTodo(@RequestBody Todo todo) {
		todoRepository.save(todo);
		//TODO validate format!!??..
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}

	@Override
	public ResponseEntity<List<Todo>> getTodos() {
		return new ResponseEntity<>(todoRepository.findAll(), HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Todo> getTodoByID(@PathVariable long id) {
		// return todoRepository.findById(id).orElseThrow(() -> new TodoNotFoundException(id));
		Optional<Todo> todoById = todoRepository.findById(id);
		if (todoById.isPresent()) {
			return new ResponseEntity<>(todoById.get(), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@Override
	public ResponseEntity<?> deleteTodo(@PathVariable long id) {
		Optional<Todo> todoById = todoRepository.findById(id);
		if (todoById.isPresent()) {
			todoRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
	}

	@Override
	public Iterable<Todo> getTodoByContentPattern(@RequestParam("pattern") String pattern) {
		return todoRepository.findAllByDescription(pattern);
	}

	@Override
	public Iterable<Todo> getTodosByDateRange(
	@RequestParam("start") long start, @RequestParam("end") long end) {
		return todoRepository.findAllByTodoDateTimeBetween(new Date(start), new Date(end));
	}

	@Override
	public Iterable<Todo> getTodosAfterDate(
	@RequestParam("start") long start) {
		return todoRepository.findAllWithTodoDateTimeAfter(new Date(start));
	}

	@Override
	public Iterable<Todo> getTodosBeforeDate(
	@RequestParam("end") long end) {
		return todoRepository.findAllWithTodoDateTimeBefore(new Date(end));
	}

}
