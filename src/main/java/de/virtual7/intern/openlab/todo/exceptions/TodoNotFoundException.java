package de.virtual7.intern.openlab.todo.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * This class is NOT used in Project
 * The Class TodoNotFoundException.
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="No such Todo")  // 404
class TodoNotFoundException extends RuntimeException {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2342355L;

  /**
   * Instantiates a new todo not found exception.
   *
   * @param todoID the todo ID
   */
  public TodoNotFoundException(long todoID) {
    super(todoID + " not found");
  }
}
