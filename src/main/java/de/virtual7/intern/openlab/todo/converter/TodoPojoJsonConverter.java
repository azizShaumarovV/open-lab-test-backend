/*
 * This Class is not used in this Project .. Devolop/Test reason
 */
package de.virtual7.intern.openlab.todo.converter;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.virtual7.intern.openlab.todo.modell.Todo;

import java.io.IOException;

/**
 * The Class TodoPojoJsonConverter.
 */
class TodoPojoJsonConverter {

    /** The object mapper. */
    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Convert to json.
     *
     * @param todo the todo
     * @return the string
     */
    public static String convertToJson(Todo todo) {
        String json = "";
        try {
            json = objectMapper.writeValueAsString(todo);
        } catch (JsonProcessingException jpe) {
            jpe.printStackTrace();
        }
        return json;
    }

    /**
     * Convert to todo entity.
     *
     * @param todoAsJson the todo as json
     * @return the todo
     */
    public static Todo convertToTodoEntity(String todoAsJson) {
        Todo todo = null;
        try {
            todo = objectMapper.readValue(todoAsJson, Todo.class);
        } catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return todo;
    }
}
