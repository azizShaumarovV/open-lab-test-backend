package de.virtual7.intern.openlab.todo.modell;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.GenerationType;
import javax.validation.constraints.Size;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Data
@Entity
@Table(name = "tbl_todo")
public class Todo {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private long id;

  @Size(max=100)
  @Column
  private String description;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date todoDateTime;


  protected Todo() {}

  public Todo(String todo) {
    this.description = todo;
  }

  public Todo(String todo, Date todoDateTime) {
    this.description = todo;
    this.todoDateTime = todoDateTime;
  }
}
