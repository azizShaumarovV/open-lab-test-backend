package de.virtual7.intern.openlab.todo.api;

import de.virtual7.intern.openlab.todo.modell.Todo;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public interface TodoControllerApi {

    @RequestMapping("*")
    @ResponseBody
    String fallbackMethod();


    @PostMapping(value = "/newtodo", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<?> newTodo(@RequestBody Todo todo);


    @GetMapping(value = "/todos", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<Todo>> getTodos();


    @GetMapping(value = "/todos/{id:[\\d]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Todo> getTodoByID(@PathVariable long id);


    @DeleteMapping(value = "/todos/{id:[\\d]+}")
    ResponseEntity<?> deleteTodo(@PathVariable long id);


    @GetMapping(value = "/todos/search",
            produces = MediaType.APPLICATION_JSON_VALUE, params = "pattern")
    Iterable<Todo> getTodoByContentPattern(
            @RequestParam("pattern") String pattern);


    @GetMapping(value = "/todos/daterange", params = {"start", "end"})
    Iterable<Todo> getTodosByDateRange(
            @RequestParam("start") long start, @RequestParam("end") long end);


    @GetMapping(value = "/todos/daterange", params = "start")
    Iterable<Todo> getTodosAfterDate(
            @RequestParam("start") long start);


    @GetMapping(value = "/todos/daterange", params = "end")
    Iterable<Todo> getTodosBeforeDate(
            @RequestParam("end") long end);
}
