package de.virtual7.intern.openlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class OpenlabApplication.
 */
@SpringBootApplication
public class OpenlabApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(OpenlabApplication.class, args);
	}
}
