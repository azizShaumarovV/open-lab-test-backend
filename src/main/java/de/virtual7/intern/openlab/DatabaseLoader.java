package de.virtual7.intern.openlab;

import de.virtual7.intern.openlab.openweathermap.WeatherService;
import de.virtual7.intern.openlab.openweathermap.data.CacheCityWeatherRepository;
import de.virtual7.intern.openlab.openweathermap.data.CityApiRepository;
import de.virtual7.intern.openlab.openweathermap.converter.CityRequestIDs;
import de.virtual7.intern.openlab.openweathermap.modell.City;

import de.virtual7.intern.openlab.todo.modell.Todo;
import de.virtual7.intern.openlab.todo.data.TodoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class DatabaseLoader implements CommandLineRunner {

    /**
     * The todo repository.
     */
    @Autowired
    public final TodoRepository todoRepository;

    /**
     * The city connectors ID repository. (All cities from openweathermap)
     */
    @Autowired
    public final CityApiRepository cityApiRepository;

    /**
     * The city Weather information cache repository.
     */
    @Autowired
    public final CacheCityWeatherRepository cacheCityWeatherRepository;

    @Autowired
    WeatherService weatherService;


    public DatabaseLoader(TodoRepository repository, CityApiRepository cityApiRepository,
                          CacheCityWeatherRepository cacheCityWeatherRepository) {
        this.todoRepository = repository;
        this.cityApiRepository = cityApiRepository;
        this.cacheCityWeatherRepository = cacheCityWeatherRepository;
    }


    @Override
    public void run(String... strings) {

        this.todoRepository.save(new Todo("chillen", new Timestamp(1538124058000L)));
        this.todoRepository.save(new Todo("joggen", new Timestamp(1539124058000L)));

        // Reads from JSON file all cities (resources/openweathermap) and save then into the CityApiRepository
        ClassLoader classLoader = getClass().getClassLoader();
        City[] citiesApi = CityRequestIDs.getCities(classLoader.getResource("openweathermap/city.list.json").getPath());

        for (City cityApi : citiesApi) {
            this.cityApiRepository.save(cityApi);
        }
        ///////////////////////////



        System.out.println(new Timestamp(System.currentTimeMillis()) + " Database loader is finished!");

    }
}
